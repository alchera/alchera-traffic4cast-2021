# Alchera Traffic4cast 2021
![plot](/README_figures/alchera.png)

Alchera Data Technologies Ltd Traffic4cast 2021 Code 

This repository contains code submitted for IARAI's Traffic4Cast 2021 Challenge.

The following link provides details on data for the traffic4cast competition:
https://github.com/iarai/NeurIPS2021-traffic4cast

Given an input of 12 frames for one hour, the following model predicts the following 6 frames for next hour.

![plot](/README_figures/model_structure.png)

## Repo Structure 
- Binary_Mask: Store PNGs for binary mask of each city 
- CoreChallenge_Mask: Store matrices for mask of each city in Core Challenge
- CoreChallenge_Model: Store model weights for Core Challenge
- ExtendedChallenge_Mask: Store matrices for mask of each city in Extended Challenge
- ExtendedChallenge_Model: Store model weights for Extended Challenge
- core_predict.py: make predictions for Core Challenge
- extended_predict.py: make predictions for Extended Challenge
- dataloader.py: standard dataloader of tensorflow 
- generate_mask.py: generate mask for test slots 
- model.py: Store tensorflow keras models for Core Challenge and Extended Challenge
- submission.py: Instructions to run the code from the model checkpoints. (suggested by IARAI)
- train.py: train models
- utilities.py: Provides various utility function

## Step
### 1. Install relevant packaged with 
`pip install -r requirements.txt`

### 2. Set the Base Path for the competition data in dataloader.py
Competiton Data Structure 
- traffic4cast2021 (base path)
    - AdditionalData
    - CoreChallenge
    - ExtendedChallenge

`BASE_PATH = '<insert path to traffic4cast folder>'`


### 3. Instructions to run the code from the saved model weights. run `python submission.py`.
Make sure all related arguments are right. 
- --base path. Default is `'/data/traffic4cast2021'`
- --core-save-path is path save predictions of core challenge. Default is `'/data/traffic4cast2021/CoreChallenge/submissions'`
- --extended-save-path is path save predictions of extended challenge. Default is `'/data/traffic4cast2021/ExtendedChallenge/submissions'`

Submission Folder (Core Challenge)        
- submissions
    - BERLIN
       - BERLIN_test_temporal.h5
    - CHICAGO
       - CHICAGO_test_temporal.h5
    - ISTANBUL
       - ISTANBUL_test_temporal.h5
    - MELBOURNE
       - MELBOURNE_test_temporal.h5

Submission Folder (Extended Challenge)
- submissions
    - VIENNA
       - VIENNA_test_spatiotemporal.h5
    - NEWYORK
       - NEWYORK_test_spatiotemporal.h5


### RUN EXAMPLE
`python submission.py --base-path=<insert path to traffic4cast data folder> --core-save-path=<path to submission folder of core challenge> --extended-save-path=<path to submission folder of extended challenge>`


### 4. Train a model for each challenge (Core/Extended) using python train.py with suitable arguments
The general training procedure consists of two parts. Firstly, we use all additional data (4 cities + 2019 and 2020) to train model. Then, we freeze temporal encoders and fine tune spatital encoders on each of 4 Core Challenge cities. This will result in 4 different models.
- --challenge-type = CoreChallenge or ExtendedChallenge. Default is `CoreChallenge`
- --base-path is same as `BASE_PATH` in Step 2 & 3. Default is `/data/traffic4cast2021`
- --save-path is the path to save model to. Default is `CoreChallenge_Model`
- --warmup-epochs is number of epochs to warmup training. Default is 25
- --finetune-epochs is number of epochs to finetune training. Default is 5.

### RUN EXAMPLE
`python train.py --challenge-type=CoreChallenge --base-path=<insert path to traffic4cast data folder> --save-path=<insert path to save trained models>`


### 5. Generate binary mask for each city using `python generate_mask.py`
Store these matrices of masks in `/CoreChallenge_Mask` or `/ExtendedChallenge_Mask` and store PNG for visualization in `/Binary_Mask`

Make sure all related parameters are right.
- `CITY = 'BERLIN'/'CHICAGO'/....`
- For CoreChallenge, 
                     
                     `file = BASE_PATH + 'CoreChallenge/' + CITY + '/' + '{}_test_temporal.h5'.format(CITY)`

                     ` with open('CoreChallenge_Mask/{}_mask.npy'.format(CITY), 'wb') as f:`



  For ExtendedChallenge, 
  
                     `file = BASE_PATH + 'ExtendedChallenge/' + CITY + '/' + '{}_test_spatiotemporal.h5'.format(CITY)`

                     ` with open('ExtendedChallenge_Mask/{}_mask.npy'.format(CITY), 'wb') as f:`

- BASE_PATH should be consistent with Step 2 & Step 3. default is `BASE_PATH = '<insert path to traffic4cast folder>'`


## 6. Make predictions for each city using `python core_predict.py` for Core Challenge or `python extended_predict.py` for Extended Challenge
Make sure all related arguments are right.
- --base-path. Default is `/data/traffic4cast2021` 
- --model-path is the path to trained models. Default is `CoreChallenge_Model` 
- --city indicates predictions for which city. Default is `'BERLIN'`.
- --save-path is the path to save predictions. default is `'Core_Predictions'`

### RUN EXAMPLE
`python core_predict.py --base-path=<insert path to traffic4cast data folder> --city=BERLIN --model-path=CoreChallenge_Model --submission-path=<insert path to save predictions>`


## Contact
Should you need help, do not hesitate to contact us: jay@alcheratechnologies.com
