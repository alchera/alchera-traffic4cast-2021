'''
Filename: train.py
Author: Jay Santokhi (jay@alcheratechnologies.com) Yiming Yang (yiming@alcheratechnologies.com) 
Usage: python train --challenge_type --data_root_path --model_output_path --warmup_epochs --finetune_epochs
Notes: Defines the training process for core and extended challenges
'''

import argparse
import os
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from model import Traffic4castModel
from dataloader import Traffic4castDataset
from tensorflow.keras.optimizers import Adam

# Configuration
parser = argparse.ArgumentParser()
parser.add_argument('--challenge-type', default='CoreChallenge', help='Training Process for which types of Challenge (CoreChallenge or ExtendedChallenge)', type=str)
parser.add_argument('--base-path', default='/data/traffic4cast2021', help='root path containing data folders', type=str)
parser.add_argument('--save-path', default='CoreChallenge_Model', help='path to save model to', type=str)
parser.add_argument('--warmup-epochs', default=25, help='number of epochs to warmup training', type=int)
parser.add_argument('--finetune-epochs', default=5, help='number of epochs to finetune training', type=int)
args = parser.parse_args()




# Create a MirroredStrategy.
strategy = tf.distribute.MirroredStrategy()
print('Number of devices: {}'.format(strategy.num_replicas_in_sync))

# Construct Model
print("[INFO] Building Model For {} Challenge".format(args.challenge_type))

# Open a Strategy Scope.
with strategy.scope():
    # Everything that creates variables should be under the strategy scope.
    # In general this is only model construction & `compile()`.
    if args.challenge_type == 'CoreChallenge':
        model = Traffic4castModel.CoreChallengeModel()
        type = args.challenge_type
        out_time_step = 6

    elif args.challenge_type == 'ExtendedChallenge':
        model = Traffic4castModel.ExtendedChallengeModel()
        type = args.challenge_type
        out_time_step = 12

    model.compile(loss="mse", optimizer=Adam(1.5e-3))

if os.path.isdir(args.save_path):
    pass
else:
    os.mkdir(args.save_path)

# WarmUp Training
print("[INFO] Loading Data For Warmup Training")
warmup_cities=['AdditionalData/ANTWERP','AdditionalData/BARCELONA','AdditionalData/BANGKOK','AdditionalData/MOSCOW']

warmup_training_ds = Traffic4castDataset(batch_size=4, sub_set='training', cities=warmup_cities, in_length=12,
                                  out_length=12, mode='overlapping', year='2019/2020', path=args.base_path + '/', out_time_step=out_time_step)                       
H = model.fit(warmup_training_ds, batch_size=4, epochs=args.warmup_epochs, verbose=1) 
model.save(args.save_path + '/' +'WarmUp_Test.hdf5')

# Plot WarmUp history: MSE
plt.plot(H.history['loss'], label='WarmUp MSE (training data)')
plt.title('MSE Loss for WarmUp')
plt.ylabel('MSE value')
plt.xlabel('No. epoch')
plt.legend(loc="upper left")
plt.savefig('warmup_loss.png')



# Freeze Temporal Encoders
print('[INFO] Freezing Layers')
Freeze_Layrers = ["temporalconvLSTM_1", "temporalconvLSTM_2", "temporalconvLSTM_3"]
for layer in Freeze_Layrers:
    layer = model.get_layer(layer)
    layer.trainable = False



# FineTune Training
print("[INFO] Loading Data For FineTune Training")
finetune_cities=['BERLIN', 'CHICAGO', 'ISTANBUL','MELBOURNE']
for city in finetune_cities:
    finetune_training_ds = Traffic4castDataset(batch_size=4, sub_set='training', cities=['CoreChallenge/'+city], in_length=12,
                            out_length=12, mode='non-overlapping', year='2019/2020', path=args.base_path + '/', out_time_step=out_time_step)
                        
    H = model.fit(finetune_training_ds, batch_size=4, epochs=args.finetune_epochs, verbose=1) 
    model.save(args.save_path + '/' + 'FineTune_{}_Test.hdf5'.format(city))

    # Plot FineTune history: MSE
    plt.plot(H.history['loss'], label='FineTune-{} MSE (training data)'.format(city))
    plt.title('MSE Loss for FineTune-{}'.format(city))
    plt.ylabel('MSE value')
    plt.xlabel('No. epoch')
    plt.legend(loc="upper left")
    plt.savefig('FineTune-{}_loss.png'.format(city))
