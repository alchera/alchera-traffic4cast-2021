'''
Filename: generate_mask.py
Author: Jay Santokhi (jay@alcheratechnologies.com)
Usage: python generate_mask.py
Notes: Run in 'traffic4cast' virtual environment
'''

import numpy as np
import matplotlib.pyplot as plt
from utilities import load_h5_file


BASE_PATH = '/data/traffic4cast2021/'
CITY = 'BERLIN'
mask = np.zeros(shape=(495, 436, 8))
count = 1

file = BASE_PATH + 'CoreChallenge/' + CITY + '/' + '{}_test_temporal.h5'.format(CITY)
data = load_h5_file(file)
data = data[:,:,:,:,0:8]

# loop through the test file
for i in range(100):
    print('Number of test slots:', count, 'of', '100')
    tmp_data = data[i]
    tmp_data = np.mean(tmp_data, axis=0)

    tmp_data[tmp_data > 0] = 1

    mask = np.logical_or(mask, tmp_data)
    mask = mask*1
    count += 1

mask_to_save = np.repeat(mask[np.newaxis,...], 6, axis=0)
print(mask_to_save.shape)
with open('CoreChallenge_Mask/{}_mask.npy'.format(CITY), 'wb') as f:
    np.save(f, mask_to_save)

fig = plt.figure(figsize=(25,22.5))
mask = mask*255
plt.imshow(np.sum(mask,axis=2), cmap='gray')
plt.xticks([])
plt.yticks([])
plt.show()
plt.savefig('Binary_Mask/{}_MASK.png'.format(CITY))