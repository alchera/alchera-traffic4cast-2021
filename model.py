'''
Filename: model.py
Author: Jay Santokhi (jay@alcheratechnologies.com) Yiming Yang (yiming@alcheratechnologies.com) Dylan Hillier (dylan@alcheratechnologies.com)
Usage: N/A
Notes: Defines the models for the traffic4cast core and extended challenge
'''
import tensorflow as tf
from tensorflow.keras.layers import Add
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Conv3D
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Reshape
from tensorflow.keras.layers import ConvLSTM2D
from tensorflow.keras.layers import MaxPooling3D
from tensorflow.keras.layers import RepeatVector
from tensorflow.keras.layers import Conv3DTranspose
from tensorflow.keras.layers import SpatialDropout3D
from tensorflow.keras.models import Model


class Traffic4castModel:
    def CoreChallengeModel():
        '''
        Model for CoreChallenge: ConvLSTM U-NET with encoder-forecasting style skip-connections 

        Number of Filters: Spatial-Encoder 8->16->32 Temporal-Encoder 8->16->32 Decoder 32->16->8
        Filter Size: Spatial-Encoder 7*7->5*5->3*3 Temporal-Encoder 5*5->5*5->5*5 Decoder 3*3->5*5->7*7

        Reference:
        ConvLSTM: X.J. Shi et al. (2015) Convolutional LSTM Network: A Machine Learning Approach for Precipitation Nowcasting
                  https://proceedings.neurips.cc/paper/2015/file/07563a3fe3bbe7e3ba84431ad9d055af-Paper.pdf

        SpatialDropout: T. Jonathan et al. (2015) Efficient Object Localization Using Convolutional Networks
                  https://arxiv.org/pdf/1411.4280.pdf

        '''
        inputShape = (12, 495, 436, 8)
        numFilters = 8
        inputs = Input(shape=inputShape)
        

        # Define Spatial Encoder
        enc_conv_lstm_1, h_enc1_1, c_enc1_1 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            name = "spatialconvLSTM_1", return_sequences=True, return_state=True)(inputs)

        pool1 = MaxPooling3D((1, 2, 2), padding='valid')(enc_conv_lstm_1)

        enc_conv_lstm_2, h_enc1_2, c_enc1_2 = ConvLSTM2D(filters=numFilters*2, kernel_size=(5, 5), padding='same',
                            name = "spatialconvLSTM_2", return_sequences=True, return_state=True)(pool1)

        pool2 = MaxPooling3D((1, 2, 2), padding='valid')(enc_conv_lstm_2)                    

        _, h_enc1_3, c_enc1_3 = ConvLSTM2D(filters=numFilters*4, kernel_size=(3, 3), padding='same',
                            name = "spatialconvLSTM_3", return_sequences=False, return_state=True)(pool2)

        # flatten, repeat and reshape
        latentDim = Flatten()(c_enc1_3)
        x = RepeatVector(12)(latentDim)
        x = Reshape((12, 123, 109, numFilters*4))(x)

        # Define Temporal Encoder
        enc2_conv_lstm_1 = ConvLSTM2D(filters=numFilters, kernel_size=(5, 5), padding='same', name="temporalconvLSTM_1",
                            return_sequences=True, return_state=False)(inputs)

        enc2_pool1 = MaxPooling3D((1, 2, 2), padding='valid')(enc2_conv_lstm_1)

        enc2_conv_lstm_2 = ConvLSTM2D(filters=numFilters*2, kernel_size=(5, 5), padding='same', name="temporalconvLSTM_2",
                            return_sequences=True, return_state=False)(enc2_pool1)

        enc2_pool2 = MaxPooling3D((1, 2, 2), padding='valid')(enc2_conv_lstm_2)                    

        _, _, c_enc2 = ConvLSTM2D(filters=numFilters*4, kernel_size=(5, 5), padding='same',  name="temporalconvLSTM_3",
                            return_sequences=False, return_state=True)(enc2_pool2)

        # flatten, repeat and reshape
        latentDim2 = Flatten()(c_enc2)
        x2 = RepeatVector(12)(latentDim2)
        x2 = Reshape((12, 123, 109, numFilters*4))(x2)

        x = Add()([x, x2])

        # Spatial Dropout & Conv3D
        x = SpatialDropout3D(0.2, data_format='channels_first')(x)
        x = Conv3D(filters=6, kernel_size=(1,1,numFilters*4), activation='relu', padding="same", data_format='channels_first')(x)

        # Define Decoder

        dec_conv_lstm_1 = ConvLSTM2D(filters=numFilters*4, kernel_size=(3, 3), padding='same',
                            return_sequences=True, return_state=False, name="deconvLSTM_1")(x,initial_state=(h_enc1_3,c_enc1_3))

        upsample1 = Conv3DTranspose(filters=numFilters, kernel_size=(1, 2, 2),
                                                   strides=(1,2,2), output_padding=(0, 1, 0))(dec_conv_lstm_1)                   

        dec_conv_lstm_2 = ConvLSTM2D(filters=numFilters*2, kernel_size=(5, 5), padding='same',
                            return_sequences=True, return_state=False, name="deconvLSTM_2")(upsample1,initial_state=(h_enc1_2,c_enc1_2))

        upsample2 = Conv3DTranspose(filters=numFilters, kernel_size=(1, 2, 2),
                                                   strides=(1,2,2), output_padding=(0, 1, 0))(dec_conv_lstm_2)                    

        dec_conv_lstm_3 = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False, name="deconvLSTM_3")(upsample2,initial_state=(h_enc1_1,c_enc1_1))

        # build and return model
        model = Model(inputs=inputs, outputs=dec_conv_lstm_3)
        print(model.summary())
        return model



    def ExtendedChallengeModel():
        '''
        Model for CoreChallenge: ConvLSTM U-NET with encoder-forecasting style skip-connections without SpatialDropout

        Number of Filters: Spatial-Encoder 8->8->8 Temporal-Encoder 8->8->8 Decoder 8->8->8
        Filter Size: Spatial-Encoder 7*7->5*5->3*3 Temporal-Encoder 5*5->5*5->5*5 Decoder 3*3->5*5->7*7

        Reference:
        ConvLSTM: X.J. Shi et al. (2015) Convolutional LSTM Network: A Machine Learning Approach for Precipitation Nowcasting
                  https://proceedings.neurips.cc/paper/2015/file/07563a3fe3bbe7e3ba84431ad9d055af-Paper.pdf

        '''
        inputShape = (12, 495, 436, 8)
        numFilters = 8
        inputs = Input(shape=inputShape)

        # Define Spatial Encoder
        enc_conv_lstm_1, h_enc1_1, c_enc1_1 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            name='spatialconvLSTM_1', return_sequences=True, return_state=True)(inputs)

        pool1 = MaxPooling3D((1, 2, 2), padding='valid')(enc_conv_lstm_1)

        enc_conv_lstm_2, h_enc1_2, c_enc1_2 = ConvLSTM2D(filters=numFilters, kernel_size=(5, 5), padding='same',
                            name='spatialconvLSTM_2', return_sequences=True, return_state=True)(pool1)

        pool2 = MaxPooling3D((1, 2, 2), padding='valid')(enc_conv_lstm_2)                    

        _, h_enc1_3, c_enc1_3 = ConvLSTM2D(filters=numFilters, kernel_size=(3, 3), padding='same',
                            name='spatialconvLSTM_3', return_sequences=False, return_state=True)(pool2)
        
        # flatten, repeat and reshape
        x = Flatten()(c_enc1_3)
        x = RepeatVector(12)(x)
        x = Reshape((12, 123, 109, numFilters))(x)

        # Define Temporal Encoder
        enc2_conv_lstm_1 = ConvLSTM2D(filters=numFilters, kernel_size=(5, 5), padding='same',
                            name="temporalconvLSTM_1", return_sequences=True, return_state=False)(inputs)

        enc2_pool1 = MaxPooling3D((1, 2, 2), padding='valid')(enc2_conv_lstm_1)

        enc2_conv_lstm_2 = ConvLSTM2D(filters=numFilters, kernel_size=(5, 5), padding='same',
                            name="temporalconvLSTM_2", return_sequences=True, return_state=False)(enc2_pool1)

        enc2_pool2 = MaxPooling3D((1, 2, 2), padding='valid')(enc2_conv_lstm_2)                    

        _, _, c_enc2 = ConvLSTM2D(filters=numFilters, kernel_size=(5, 5), padding='same',
                            name="temporalconvLSTM_3", return_sequences=False, return_state=True)(enc2_pool2)

        # flatten, repeat and reshape
        x2 = Flatten()(c_enc2)
        x2 = RepeatVector(12)(x2)
        x2 = Reshape((12, 123, 109, numFilters))(x2)

        x = Add()([x, x2]) 
        
        # Define Decoder
        dec_conv_lstm_1 = ConvLSTM2D(filters=numFilters, kernel_size=(3, 3), padding='same', name="deconvLSTM_1",
                            return_sequences=True, return_state=False)(x,initial_state=(h_enc1_3,c_enc1_3))

        upsample1 = Conv3DTranspose(filters=numFilters, kernel_size=(1, 2, 2),
                                                   strides=(1,2,2), output_padding=(0, 1, 0))(dec_conv_lstm_1)                   

        dec_conv_lstm_2 = ConvLSTM2D(filters=numFilters, kernel_size=(5, 5), padding='same', name="deconvLSTM_2",
                            return_sequences=True, return_state=False)(upsample1,initial_state=(h_enc1_2,c_enc1_2))

        upsample2 = Conv3DTranspose(filters=numFilters, kernel_size=(1, 2, 2),
                                                   strides=(1,2,2), output_padding=(0, 1, 0))(dec_conv_lstm_2)                    

        dec_conv_lstm_3 = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same', name="deconvLSTM_3",
                            return_sequences=True, return_state=False)(upsample2,initial_state=(h_enc1_1,c_enc1_1))

        # build and return model
        model = Model(inputs=inputs, outputs=dec_conv_lstm_3)
        print(model.summary())
        return model