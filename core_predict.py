'''
Filename: core_predict.py
Author: Jay Santokhi (jay@alcheratechnologies.com) Yiming Yang (yiming@alcheratechnologies.com) 
Usage: python core_predict.py --test_file_path --model_path --city --submission_path
Notes: Defines the prediction process for core challenges
'''
import argparse
import os
import numpy as np
import tensorflow as tf
from tensorflow_addons.optimizers import LAMB
from utilities import postprocess
from utilities import load_mask
from utilities import save_h5_file
from utilities import load_h5_file


# Configuration
parser = argparse.ArgumentParser()
parser.add_argument('--base-path', default='/data/traffic4cast2021', help='root path containing data folders', type=str)
parser.add_argument('--model-path', default='CoreChallenge_Model', help='path to save model to', type=str)
parser.add_argument('--city', default='BERLIN', help='predict for which city in core challenge (BERLIN, CHICAGO, ISTANBUL, MELBOURNE)', type=str)
parser.add_argument('--save-path', default='Core_Predictions', help='Path to save predictions', type=str)

args = parser.parse_args()

if os.path.isdir(args.save_path):
    pass
else:
    os.mkdir(args.save_path)

save_path = args.save_path + '/' + args.city 

if os.path.isdir(save_path):
    pass
else:
    os.mkdir(save_path)

CITY = args.city
base_path = args.base_path


print("[INFO] Loading Models")
model = tf.keras.models.load_model(args.model_path + '/' + 'FineTune_{}.hdf5'.format(CITY))


print("[INFO] Loading Masks")
mask = load_mask('CoreChallenge_Mask/{}_mask.npy'.format(CITY))


print("[INFO] Loading Test Files")
file =  base_path + '/' + 'CoreChallenge' + '/' + CITY + '/' + CITY + '_test_temporal.h5'
print(file)
x_test = load_h5_file(file)
print(x_test.shape)


no_of_predictions = x_test.shape[0]
predictions = np.zeros((x_test.shape[0], 6, 495, 436, 8))

# preprocessing
x_test = x_test.astype(np.float32)
x_test /= 255.



print("[INFO] Start Predictions")
for i in range(no_of_predictions):
    X_test = np.expand_dims(x_test[i,:,:,:,:], axis=0)
    # print(X_test.shape)
    raw_preds = model.predict(X_test, batch_size=1, verbose=1)

    # post process
    processed_preds = postprocess(raw_preds)
    b_mask = np.repeat(mask[np.newaxis,...], no_of_predictions, axis=0)
    processed_preds = np.multiply(processed_preds, b_mask)

    processed_preds = processed_preds.astype(np.uint8)

    predictions[i,:,:,:,:] = processed_preds[0]
    print('{} Predictions Complete'.format(i+1))

# save the predicted tensor to directories of predictions in the correct format
save_path = save_path + '/' + CITY + '_test_temporal.h5'
print(save_path)
# save_h5_file(submission_path, processed_preds)
print("[INFO] Writing Predictions to File")
save_h5_file(save_path, predictions)
