'''
Filename: submission.py
Author: Jay Santokhi (jay@alcheratechnologies.com) Yiming Yang (yiming@alcheratechnologies.com) 
Usage: python submission.py --input_file_path --challenge_type --city --save_path 
Notes: Instructions to run the code from the model checkpoints. 
'''
import argparse
import os
import numpy as np
import tensorflow as tf
from utilities import postprocess
from utilities import load_mask
from utilities import save_h5_file
from utilities import load_h5_file
from tensorflow_addons.optimizers import LAMB

# Configuration
parser = argparse.ArgumentParser()
parser.add_argument('--base-path', default='/data/traffic4cast2021', help='Path to competition data folder', type=str)
parser.add_argument('--core-save-path', default='/data/traffic4cast2021/CoreChallenge/submissions', help='Path to save Core Challenge Predictions', type=str)
parser.add_argument('--extended-save-path', default='/data/traffic4cast2021/ExtendedChallenge/submissions', help='Path to save Extended Challenge Predictions', type=str)

args = parser.parse_args()

def CoreChallenge_Predict(input_file_path, city, save_path):
    print("[INFO] Loading Models")
    model = tf.keras.models.load_model('CoreChallenge_Model/FineTune_{}.hdf5'.format(city))

    print("[INFO] Loading Masks")
    mask = load_mask('CoreChallenge_Mask/{}_mask.npy'.format(city))
    

    print("[INFO] Loading Test Files")
    # path = BASE_PATH + CITY + '/' + 'testing'
    file =  input_file_path
    print(file)
    x_test = load_h5_file(file)
    print(x_test.shape)

    no_of_predictions = x_test.shape[0]
    predictions = np.zeros((x_test.shape[0], 6, 495, 436, 8))

    # preprocessing
    x_test = x_test.astype(np.float32)
    x_test /= 255.


    print("[INFO] Start Predictions")
    for i in range(no_of_predictions):
        X_test = np.expand_dims(x_test[i,:,:,:,:], axis=0)
        # print(X_test.shape)
        raw_preds = model.predict(X_test, batch_size=1, verbose=1)

        # post process
        processed_preds = postprocess(raw_preds)
        b_mask = np.repeat(mask[np.newaxis,...], no_of_predictions, axis=0)
        processed_preds = np.multiply(processed_preds, b_mask)

        processed_preds = processed_preds.astype(np.uint8)

        predictions[i,:,:,:,:] = processed_preds[0]
        print('{} Predictions Complete'.format(i+1))
    
    # save the predicted tensor to directories of predictions in the correct format
    if os.path.isdir(save_path):
        pass
    else:
        os.mkdir(save_path)

    submission_folder = save_path + '/' + city + '/' 
    submission_path = submission_folder + city + '_test_temporal.h5'

    if os.path.isdir(submission_folder):
        pass
    else:
        os.mkdir(submission_folder)

    print(submission_path)
    print("[INFO] Writing Predictions to File")
    save_h5_file(submission_path, predictions)

def ExtendedChallenge_Predict(input_file_path, city, save_path):
    print("[INFO] Loading Models")
    model_1 = tf.keras.models.load_model('ExtendedChallenge_Model/FineTune_BERLIN.hdf5')
    model_2 = tf.keras.models.load_model('ExtendedChallenge_Model/FineTune_CHICAGO.hdf5')
    model_3 = tf.keras.models.load_model('ExtendedChallenge_Model/FineTune_ISTANBUL.hdf5')
    model_4 = tf.keras.models.load_model('ExtendedChallenge_Model/FineTune_MELBOURNE.hdf5')

    print("[INFO] Loading Masks")
    mask = load_mask('ExtendedChallenge_Mask/{}_mask.npy'.format(city))

    print("[INFO] Loading Test Files")
    file =  input_file_path
    print(file)
    x_test = load_h5_file(file)
    print(x_test.shape)

    no_of_predictions = x_test.shape[0]
    predictions = np.zeros((x_test.shape[0], 6, 495, 436, 8))

    # preprocessing
    x_test = x_test.astype(np.float32)
    x_test /= 255.


    print("[INFO] Start Predictions")
    for i in range(no_of_predictions):
        X_test = np.expand_dims(x_test[i,:,:,:,:], axis=0)
        raw_preds_1 = model_1.predict(X_test, batch_size=1, verbose=1)
        raw_preds_2 = model_2.predict(X_test, batch_size=1, verbose=1)
        raw_preds_3 = model_3.predict(X_test, batch_size=1, verbose=1)
        raw_preds_4 = model_4.predict(X_test, batch_size=1, verbose=1)

        raw_preds = (raw_preds_1 + raw_preds_2 + raw_preds_3 + raw_preds_4)/4.

        # post process
        processed_preds = postprocess(raw_preds)
        # select desired prediction time bins
        pred_idx = [0, 1, 2, 5, 8, 11]
        processed_preds = processed_preds[:, pred_idx, :, :, :]

        b_mask = np.repeat(mask[np.newaxis,...], no_of_predictions, axis=0)
        processed_preds = np.multiply(processed_preds, b_mask)

        processed_preds = processed_preds.astype(np.uint8)

        predictions[i,:,:,:,:] = processed_preds[0]
        print('{} Predictions Complete'.format(i+1))
    
    # save the predicted tensor to directories of predictions in the correct format
    if os.path.isdir(save_path):
        pass
    else:
        os.mkdir(save_path)

    submission_folder = save_path + '/' + city + '/' 


    submission_path = submission_folder + city + '_test_spatiotemporal.h5'

    if os.path.isdir(submission_folder):
        pass
    else:
        os.mkdir(submission_folder)
        
    print(submission_path)
    print("[INFO] Writing Predictions to File")
    save_h5_file(submission_path, predictions)


if __name__ == "__main__":
    base_path = args.base_path
    core_save_path = args.core_save_path
    extended_save_path = args.extended_save_path

    Core_City = ['BERLIN', 'CHICAGO', 'ISTANBUL', 'MELBOURNE']
    for city in Core_City:     
        input_file_path = base_path + '/' + 'CoreChallenge/{}/{}_test_temporal.h5'.format(city,city)
        print('[INFO] Predictions on {}'.format(city))
        print('[INFO] file path : {}'.format(input_file_path))
        CoreChallenge_Predict(input_file_path, city, core_save_path)


    Extended_City = ['VIENNA', 'NEWYORK']
    for city in Extended_City:  
        input_file_path = base_path + '/' + 'ExtendedChallenge/{}/{}_test_spatiotemporal.h5'.format(city,city)
        print('[INFO] Predictions on {}'.format(city))
        ExtendedChallenge_Predict(input_file_path, city, extended_save_path)
        


        



